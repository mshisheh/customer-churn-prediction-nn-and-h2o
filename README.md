# Customer churn prediction NN and H2O

<div align="center">
    <img src="Customer-Churn.jpg" width="800px"</img> 
</div>

## Getting started
Customer churn is the percentage of customers that stopped using your company's product or service during a certain time frame. You can calculate churn rate by dividing the number of customers you lost during that time period -- say a quarter -- by the number of customers you had at the beginning of that time period.

## 
